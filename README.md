# GNU/Linux de débutant à confirmé en quelques heures

Bonjour et bienvenue dans ce scénario créé par **Jordan ASSOULINE** pour vous aider à maîtriser les commandes permettant de manipuler les groupes sur Linux

Ainsi nous verrons en détail les commandes:
- `addgroup`: permet de créer un nouveau groupe
- `delgroup`: permet de supprimer un groupe
- `usermod`: permet de modifier, entre autre, l'appartenance des utilisateurs à des groupes
- `chown`: permet de modifier l'association d'un utilisateur à un fichier

<br/>

# Introduction

Tout d'abord, je vous invite à vérifier que Docker est bien installé sur votre machine.
Pour cela tapez la commande :
`sudo apt update && sudo apt install docker docker.io -y`

Vous pouvez aussi utiliser le docker playground si vous ne souhaitez pas installer ces éléments sur votre propre machine :
https://labs.play-with-docker.com/

Pour lancer l'environnement de l'exercice, vous allez exécuter la commande :

`docker run -it --rm --name man jassouline/groupes:latest bash`

Pour sortir de l'environnement, il suffit de taper la commande `exit` ou de taper sur la combinaison de touches : `CTRL + C`

<br/>

# Exercice 1

Dans ce premier exercice, nous allons utiliser la commande `addgroup` pour créer de nouveaux groupes.

1. Utilisez la commande `addgroup` pour créer un nouveau groupe nommé : **admins**

<details><summary> Montrer la solution </summary>
<p>
addgroup admins
</p>
</details>

Vous pouvez vérifiez que ce groupe a bien été créé grâce à la commande `tail -n 1 /etc/group` qui doit vous afficher le résultat :
```
admins:x:1000:
```

2. Utilisez la commande `addgroup` pour créer un nouveau groupe nommé : **observateurs**

<details><summary> Montrer la solution </summary>
<p>
addgroup observateurs
</p>
</details>

Vous pouvez vérifiez que ce groupe a bien été créé grâce à la commande `tail -n 1 /etc/group` qui doit vous afficher le résultat :
```
observateurs:x:1001:
```

3. Utilisez la commande `adduser` pour créer l'utilisateur **jordan** (peu importe le mot de passe).

<details><summary> Montrer la solution </summary>
<p>
adduser jordan
</p>
</details>

4. Exécutez à nouveau la commande `tail -n 1 /etc/group`

**Q1: Quel groupe vient d'être créé ?**

<details><summary> Montrer la solution </summary>
<p>
jordan
</p>
</details>

5. Grâce à la commande `groups jordan` vous pouvez voir que l'utilisateur **jordan** que vous venez de créer appartient au groupe : `jordan`.
   
6. Utilisez la commande `usermod -g admins jordan` pour ajouter l'utilisateur **jordan** au groupe **admins**.

7. Vérifiez à nouveau avec la commande `groups jordan` que l'utilisateur appartient bien au groupe **admins**.

8. Pour vérifier que vous avez correctement réussi cet exercice, tapez la commande suivante dans votre terminal :
`/tmp/addgroup_verify.sh`

Si vous obtenez le message : `OK !` c'est que les actions ont été correctement effectuées.

<br/>

# Exercice 2

Dans ce deuxième exercice, nous allons utiliser la commande `usermod` pour associer un utilisateur à plusieurs groupes.

Grâce à la commande `groups jordan` vous pouvez voir que l'utilisateur **jordan** appartient au groupe **admins**.

1. Utilisez la commande `usermod -g observateurs jordan` pour ajouter l'utilisateur **jordan** au groupe **observateurs**.

2. Vérifiez à nouveau avec la commande `groups jordan`.

On observe ainsi que l'utilisateur **jordan** appartient désormais uniquement au groupe **observateurs** qui a remplacé le groupe **admins**.

3. Grâce à la commande `man usermod`, essayez d'identifier l'option qui permet d'ajouter un utilisateur à des groupes supplémentaires, sans que ceux-ci ne remplacent les groupes actuels auxquels appartient l'utilisateur.

4. Une fois trouvé, utilisez la commande `usermod` pour associer l'utilisateur **jordan** à la fois au groupe **admins** et au groupe **observateurs**.

<details><summary> Montrer la solution </summary>
<p>
usermod -G admins jordan
</p>
</details>

# Exercice 3

Dans ce troisième exercice, nous allons utiliser les commandes `delgroup` et `chown` pour supprimer des groupes et associer des fichiers à des utilisateurs spécifiques.

Commencez par exécuter la commande suivante :
`/tmp/environnement.sh`

# Supprimer des groupes

Un groupe a été créé par inadvertance par un stagiaire. Identifiez le groupe qui vient d'être créé.

**Q1: Quel groupe vient d'être créé ?**

<details><summary> Montrer la solution </summary>
<p>
tail -n 1 /etc/group
</p>
<p>
C'est le : groupe1
</p>
</details>

1. Grâce à la commande `delgroup` vous allez supprimer le groupe que vous avez trouvé à la question précédente.

<details><summary> Montrer la solution </summary>
<p>
delgroup groupe1
</p>
</details>

**Q2: Un fichier vient d'être créé dans le répertoire `/root`, quel est son nom ?**

<details><summary> Montrer la solution </summary>
<p>
ls -l /root
</p>
<p>
C'est : fichier1
</p>
</details>

**Q3: A quel utilisateur appartient ce fichier ?**

<details><summary> Montrer la solution </summary>
<p>
ls -l /root/fichier1
</p>
<p>
Le fichier appartient à l'utilisateur root.
</p>
</details>

2. Utilisez la commande `chown` pour que le fichier appartienne à l'utilisateur **jordan**

<details><summary> Montrer la solution </summary>
<p>
chown jordan /root/fichier1
</p>
</details>

3. Pour vérifier que vous avez correctement réussi cet exercice, tapez la commande suivante dans votre terminal :
`/tmp/delgroup_verify.sh`

Si vous obtenez le message : `OK !` c'est que les actions ont été correctement effectuées.

# Conclusion

Tapez la commande `exit` pour sortir du conteneur, ou bien la combinaison de touches `CTRL + c`

